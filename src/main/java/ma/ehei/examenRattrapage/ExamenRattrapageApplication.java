package ma.ehei.examenRattrapage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExamenRattrapageApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamenRattrapageApplication.class, args);
	}

}
