package ma.ehei.examenRattrapage.Metier;
import ma.ehei.examenRattrapage.Dao.Entity.Personne;

import org.springframework.data.jpa.repository.JpaRepository;


public interface PersonneService extends JpaRepository<Personne,Long> {
	

}
