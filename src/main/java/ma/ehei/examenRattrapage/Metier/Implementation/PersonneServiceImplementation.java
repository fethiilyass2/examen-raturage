package ma.ehei.examenRattrapage.Metier.Implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ma.ehei.examenRattrapage.Dao.PersonneDao;
import ma.ehei.examenRattrapage.Dao.Entity.Personne;
import ma.ehei.examenRattrapage.Metier.PersonneService;

@Component
public class PersonneServiceImplementation implements PersonneDao {
	
	@Autowired
	private PersonneService ps;
	
	public List<Personne> findAll() {
		// TODO Auto-generated method stub
		return ps.findAll();
	}

	public Personne Insert(Personne P) {
		// TODO Auto-generated method stub
		return ps.save(P);
	}

	public Personne Update(Long Id, Personne P) {
		// TODO Auto-generated method stub
		
		Personne personne = ps.findById(Id).get();
		personne.setCin(P.getCin());
		personne.setNom(P.getNom());
		personne.setPrénom(P.getPrénom());
		
		
		return ps.saveAndFlush(personne);
	}

}
