package ma.ehei.examenRattrapage.Presentation.Controlleur;

import java.util.List;
import java.util.Optional;

import ma.ehei.examenRattrapage.Dao.PersonneDao;
import ma.ehei.examenRattrapage.Dao.Entity.Personne;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("Personne")
public class PersonneControlleur {
	
	   @Autowired
	    private PersonneDao pd;

	    @RequestMapping("/all")
	    public ResponseEntity<List<Personne>> findAll(){
	        return new ResponseEntity<>(pd.findAll() , HttpStatus.OK);
	    }

	    @PostMapping( path = "/Ajouter")
	    public ResponseEntity<Personne> create( @RequestBody Personne P){
	    	Personne personne = pd.Insert(P);
	        return new ResponseEntity<>(HttpStatus.OK);
	    }
	    @PutMapping("/Modifier/{Id}")
	    public ResponseEntity<Personne> update(@PathVariable("Id") Long Id ,@RequestBody Personne P){
	    	
	    	Personne pUpdate = pd.Update(Id, P);
	    	if(pUpdate != null){
	    		pUpdate.setCin(P.getCin());
	    		pUpdate.setNom(P.getNom());
	    		pUpdate.setPrénom(P.getPrénom());
	    	}
	    	return new ResponseEntity<>(HttpStatus.OK);	
	    }
	

}
