package ma.ehei.examenRattrapage.Dao;

import java.util.List;

import ma.ehei.examenRattrapage.Dao.Entity.Personne;

public interface PersonneDao {
	public List<Personne> findAll();
	public Personne Insert(Personne P);
	public Personne Update(Long Id, Personne P);
	
	

}
