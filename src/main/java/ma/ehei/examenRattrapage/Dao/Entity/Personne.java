package ma.ehei.examenRattrapage.Dao.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity 
public class Personne {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(unique = true)
	private String cin;
	private String nom;
	private String prénom ;
	public String getCin() {
		return cin;
	}
	public void setCin(String cin) {
		this.cin = cin;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrénom() {
		return prénom;
	}
	public void setPrénom(String prénom) {
		this.prénom = prénom;
	}
	public Long getId() {
		return id;
	}
	@Override
	public String toString() {
		return "Personne [id=" + id + ", cin=" + cin + ", nom=" + nom
				+ ", prénom=" + prénom + "]";
	}
	public Personne(Long id, String cin, String nom, String prénom) {
		super();
		this.id = id;
		this.cin = cin;
		this.nom = nom;
		this.prénom = prénom;
	}
	
	public Personne() {
		super();
	}
	
	
}


