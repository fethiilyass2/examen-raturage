create database examratt;

use examratt;

create table personne(

id bigint(20) not null auto_increment,
cin varchar(255),
nom varchar(255),
prénom varchar(255),
primary key (id)
);

alter table personne add constraint UK_uniqueCIN unique (cin)

